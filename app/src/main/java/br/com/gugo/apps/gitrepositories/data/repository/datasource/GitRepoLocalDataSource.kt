package br.com.gugo.apps.gitrepositories.data.repository.datasource

import android.arch.lifecycle.LiveData
import br.com.gugo.apps.gitrepositories.data.cache.GitRepoCache
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo

class GitRepoLocalDataSource(private val gitRepoCache: GitRepoCache) {

    fun gitRepos(): LiveData<GitRepo.List> = gitRepoCache.get()
    fun clear() = gitRepoCache.clear()
    fun insertRepo(item: GitRepo) = gitRepoCache.insert(item)
    fun putRepos(list: GitRepo.List) = gitRepoCache.put(list)
}