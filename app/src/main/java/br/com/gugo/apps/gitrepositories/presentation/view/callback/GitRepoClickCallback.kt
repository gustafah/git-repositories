package br.com.gugo.apps.gitrepositories.presentation.view.callback

import br.com.gugo.apps.gitrepositories.domain.model.GitRepo

interface GitRepoClickCallback {

    fun onItemClick(gitRepo: GitRepo)

}