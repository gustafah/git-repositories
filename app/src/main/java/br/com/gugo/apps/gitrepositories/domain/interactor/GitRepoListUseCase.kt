package br.com.gugo.apps.gitrepositories.domain.interactor

import android.arch.lifecycle.LiveData
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams

interface GitRepoListUseCase {

    fun getDataToObserve(): LiveData<GitRepo.List>
    fun searchGitRepo(params: GitRepoSearchParams?)

}