package br.com.gugo.apps.gitrepositories.presentation.view.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import br.com.gugo.apps.gitrepositories.R
import br.com.gugo.apps.gitrepositories.domain.interactor.GitRepoListInteractor
import br.com.gugo.apps.gitrepositories.domain.interactor.GitRepoListUseCase
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams
import br.com.gugo.apps.gitrepositories.presentation.view.adapter.GitRepoAdapter
import br.com.gugo.apps.gitrepositories.presentation.view.callback.GitRepoClickCallback
import br.com.gugo.apps.gitrepositories.presentation.view.custom.EndlessRecyclerViewScrollListener
import br.com.gugo.apps.gitrepositories.presentation.viewmodel.GitRepoListViewModel
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*


class GitRepoListActivity : AppCompatActivity(), GitRepoClickCallback {

    private lateinit var gitRepoAdapter: GitRepoAdapter
    private lateinit var gitRepoListUseCase: GitRepoListUseCase
    private lateinit var viewModel: GitRepoListViewModel
    private var searchParams = GitRepoSearchParams()
    private lateinit var endlessScroll: EndlessRecyclerViewScrollListener
    private var recyclerState: Parcelable? = null

    private val dataObserver = Observer<GitRepo.List> { list ->
        if (list?.size ?: 0 > 0) {
            list!!.sortWith(Comparator { o1, o2 ->
                when (searchParams.sort) {
                    GitRepoSearchParams.SORTING.STARS -> {
                        when (searchParams.order) {
                            GitRepoSearchParams.ORDER.ASC -> o1.stargazersCount.compareTo(o2.stargazersCount)
                            GitRepoSearchParams.ORDER.DESC -> o2.stargazersCount.compareTo(o1.stargazersCount)
                        }
                    }
                    GitRepoSearchParams.SORTING.FORKS -> {
                        when (searchParams.order) {
                            GitRepoSearchParams.ORDER.ASC -> o1.forksCount.compareTo(o2.forksCount)
                            GitRepoSearchParams.ORDER.DESC -> o2.forksCount.compareTo(o1.forksCount)
                        }
                    }
                }
            })
            swipeRefreshLayout.isRefreshing = false
            if (searchParams.resetCache)
                srv_repos.hideShimmerAdapter()

            if (gitRepoAdapter.list.size < list.size)
                gitRepoAdapter.addData(
                    GitRepo.List(
                        list.subList(
                            gitRepoAdapter.list.size,
                            list.size
                        )
                    )
                )
            else
                gitRepoAdapter.setData(list)
            recyclerState?.let {
                srv_repos.layoutManager?.onRestoreInstanceState(it)
            }
            recyclerState = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        setup()
        initLayout()

        if (savedInstanceState == null)
            viewModel.getGitRepoList(searchParams)
    }

    private fun setup() {
        gitRepoAdapter = GitRepoAdapter(this)
        gitRepoListUseCase = GitRepoListInteractor(this)

        viewModel = GitRepoListViewModel(gitRepoListUseCase)
        viewModel.subscribeForData(this, dataObserver)

        defaultSearch()
    }

    private fun defaultSearch() {
        with(searchParams) {
            term = "language:kotlin"
            perPage = GitRepoSearchParams.PER_PAGE
            page = 1
            sort = GitRepoSearchParams.SORTING.STARS
            order = GitRepoSearchParams.ORDER.DESC
            resetCache = true
        }
    }

    private fun initLayout() {
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(getColor(R.color.white))

        tv_search.text = searchParams.term

        val linearLayoutManager = LinearLayoutManager(this)
        endlessScroll = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                loadNextPage(page)
            }
        }
        with(srv_repos) {
            adapter = gitRepoAdapter
            layoutManager = linearLayoutManager
            addOnScrollListener(endlessScroll)
        }
        swipeRefreshLayout.setOnRefreshListener {
            refreshList()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_scrolling, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> {
                showFilterDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        srv_repos.layoutManager?.let {
            outState?.putParcelable(srv_repos::class.java.name, it.onSaveInstanceState())
        }
        outState?.putParcelable(searchParams::class.java.name, searchParams)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.let { bundle ->
            recyclerState = bundle.getParcelable(srv_repos::class.java.name)
            searchParams = bundle.getParcelable(searchParams::class.java.name)!!
        }
    }

    override fun onItemClick(gitRepo: GitRepo) {
        println(gitRepo)
    }

    fun loadNextPage(page: Int) {
        swipeRefreshLayout.isRefreshing = true
        searchParams.resetCache = false
        searchParams.page++
        viewModel.getGitRepoList(
            searchParams
        )
    }

    fun refreshList() {
        gitRepoAdapter.clear()
        endlessScroll.resetState()
        searchParams.page = 1
        searchParams.resetCache = true
        swipeRefreshLayout.isRefreshing = false
        srv_repos.showShimmerAdapter()
        viewModel.getGitRepoList(
            searchParams
        )
    }

    private fun showFilterDialog() {
        val dialogBuilder = AlertDialog.Builder(this, R.style.AppTheme_Dialog)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_filter, linearLayout, false)
        dialogBuilder.setView(dialogView)
        dialogBuilder.setTitle(R.string.action_filter)
        val etTerm = dialogView.findViewById<EditText>(R.id.et_term)
        val rdgSort = dialogView.findViewById<RadioGroup>(R.id.rdg_sort)
        val rdgOrder = dialogView.findViewById<RadioGroup>(R.id.rdg_order)
        val rdbStars = dialogView.findViewById<RadioButton>(R.id.rdb_stars)
        val rdbForks = dialogView.findViewById<RadioButton>(R.id.rdb_forks)
        val rdbAsc = dialogView.findViewById<RadioButton>(R.id.rdb_asc)
        val rdbDesc = dialogView.findViewById<RadioButton>(R.id.rdb_desc)

        etTerm.setText(searchParams.term)
        when (searchParams.sort) {
            GitRepoSearchParams.SORTING.STARS -> rdbStars.isChecked = true
            GitRepoSearchParams.SORTING.FORKS -> rdbForks.isChecked = true
        }
        when (searchParams.order) {
            GitRepoSearchParams.ORDER.ASC -> rdbAsc.isChecked = true
            GitRepoSearchParams.ORDER.DESC -> rdbDesc.isChecked = true
        }

        dialogBuilder.setPositiveButton(
            getString(R.string.text_ok)
        ) { dialog, id ->
            if (etTerm.text.isNotEmpty()) {
                searchParams.term = etTerm.text.toString()
                tv_search.text = etTerm.text
                when (rdgSort.checkedRadioButtonId) {
                    rdbStars.id -> {
                        searchParams.sort = GitRepoSearchParams.SORTING.STARS
                    }
                    rdbForks.id -> {
                        searchParams.sort = GitRepoSearchParams.SORTING.FORKS
                    }
                }
                when (rdgOrder.checkedRadioButtonId) {
                    rdbAsc.id -> {
                        searchParams.order = GitRepoSearchParams.ORDER.ASC
                    }
                    rdbDesc.id -> {
                        searchParams.order = GitRepoSearchParams.ORDER.DESC
                    }
                }
                refreshList()
                dialog.dismiss()
            }
        }
        dialogBuilder.setNegativeButton(
            getString(R.string.text_cancel)
        ) { dialog, which -> dialog.dismiss() }

        val alertDialog = dialogBuilder.create()
//        alertDialog.window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        alertDialog.show()
    }
}
