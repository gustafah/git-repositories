package br.com.gugo.apps.gitrepositories.domain.interactor

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.content.Context
import br.com.gugo.apps.gitrepositories.data.cache.GitRepoCacheImpl
import br.com.gugo.apps.gitrepositories.data.network.createNetworkClient
import br.com.gugo.apps.gitrepositories.data.network.interfaces.GitRepoApi
import br.com.gugo.apps.gitrepositories.data.repository.GitRepoDataRepository
import br.com.gugo.apps.gitrepositories.data.repository.datasource.GitRepoLocalDataSource
import br.com.gugo.apps.gitrepositories.data.repository.datasource.GitRepoRemoteDataSource
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams
import br.com.gugo.apps.gitrepositories.domain.repository.GitRepoRepository
import io.reactivex.schedulers.Schedulers.io

class GitRepoListInteractor(val context: Context) : GitRepoListUseCase {

    private val repository: GitRepoRepository = GitRepoDataRepository(
        GitRepoLocalDataSource(GitRepoCacheImpl(context)),
        GitRepoRemoteDataSource(
            createNetworkClient("https://api.github.com", false).create(
                GitRepoApi::class.java
            )
        )
    )

    override fun getDataToObserve(): LiveData<GitRepo.List> {
        return repository.getLiveData()
    }

    @SuppressLint("CheckResult")
    override fun searchGitRepo(params: GitRepoSearchParams?) {
        repository.searchGitRepo(params).subscribeOn(io()).observeOn(io()).doOnComplete { }
            .subscribe {
                if (params?.resetCache == true) repository.clearCache()
                repository.cacheData(it)
            }
    }
}