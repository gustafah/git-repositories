package br.com.gugo.apps.gitrepositories.data.cache

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.content.Context
import br.com.gugo.apps.gitrepositories.data.cache.db.GitRepoDao
import br.com.gugo.apps.gitrepositories.data.cache.db.GitRepoDatabase
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo

class GitRepoCacheImpl(context: Context) : GitRepoCache {
    private var gitRepoDao: GitRepoDao

    init {
        val db: GitRepoDatabase = GitRepoDatabase.getDatabase(context)
        this.gitRepoDao = db.gitRepoDao()!!
    }

    override fun get(): LiveData<GitRepo.List> {
        return LiveDataReactiveStreams.fromPublisher(this.gitRepoDao.getRepos().map {
            GitRepo.List(it)
        })
    }

    override fun insert(gitRepo: GitRepo) {
        gitRepo.ownerName = gitRepo.owner.login
        gitRepo.ownerPicture = gitRepo.owner.avatarUrl
        this.gitRepoDao.insertGitRepo(gitRepo)
    }

    override fun put(gitRepos: GitRepo.List) {
        gitRepos.forEach {
            insert(it)
        }
//        this.gitRepoDao.insertGitRepoList(gitRepos)
    }

    override fun clear() {
        gitRepoDao.deleteAll()
    }
}