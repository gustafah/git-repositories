package br.com.gugo.apps.gitrepositories.presentation.viewmodel

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import br.com.gugo.apps.gitrepositories.domain.interactor.GitRepoListUseCase
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams

class GitRepoListViewModel(private val useCase: GitRepoListUseCase) :
    ViewModel() {

    fun subscribeForData(lifecyleOwner: LifecycleOwner, observer: Observer<GitRepo.List>) {
        useCase.getDataToObserve().observe(lifecyleOwner, observer)
    }

    fun getGitRepoList(params: GitRepoSearchParams) {
        useCase.searchGitRepo(params)
    }

}