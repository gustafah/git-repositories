package br.com.gugo.apps.gitrepositories.domain.model

import com.google.gson.annotations.SerializedName

data class GitRepoOwner(
    @SerializedName("avatar_url") var avatarUrl: String = "",
    @SerializedName("id") var id: Int = 0,
    @SerializedName("login") var login: String = ""
)