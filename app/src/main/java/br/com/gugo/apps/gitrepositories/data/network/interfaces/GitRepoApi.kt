package br.com.gugo.apps.gitrepositories.data.network.interfaces

import br.com.gugo.apps.gitrepositories.data.entity.GitRepoApiResultEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface GitRepoApi {

    @GET("search/repositories")
    fun getGitRepos(
        @Query("q") q: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int,
        @Query("sort") sort: String,
        @Query("order") order: String
    ): Observable<GitRepoApiResultEntity>

    @GET("search/repositories")
    fun getGitRepos(): Observable<GitRepoApiResultEntity>

}