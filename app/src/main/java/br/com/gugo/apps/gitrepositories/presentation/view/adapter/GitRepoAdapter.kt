package br.com.gugo.apps.gitrepositories.presentation.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.gugo.apps.gitrepositories.R
import br.com.gugo.apps.gitrepositories.databinding.ItemRepoBinding
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.presentation.view.callback.GitRepoClickCallback

class GitRepoAdapter(private val gitRepoClickCallback: GitRepoClickCallback) :
    RecyclerView.Adapter<GitRepoAdapter.GitRepoViewHolder>() {

    var list: GitRepo.List = GitRepo.List()

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(vh: GitRepoAdapter.GitRepoViewHolder, position: Int) {
        vh.binding.gitRepo = list[position]
        vh.binding.executePendingBindings()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GitRepoAdapter.GitRepoViewHolder {
        val binding: ItemRepoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_repo,
            parent,
            false
        )
        binding.callback = gitRepoClickCallback
        return GitRepoViewHolder(binding)
    }

    fun addData(data: GitRepo.List) {
        list.addAll(data)
        notifyDataSetChanged()
    }

    fun setData(data: GitRepo.List) {
        list.clear()
        list = data
        notifyDataSetChanged()
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }

    inner class GitRepoViewHolder(val binding: ItemRepoBinding) :
        RecyclerView.ViewHolder(binding.root)
}