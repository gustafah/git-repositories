package br.com.gugo.apps.gitrepositories.data.repository

import android.arch.lifecycle.LiveData
import br.com.gugo.apps.gitrepositories.data.repository.datasource.GitRepoLocalDataSource
import br.com.gugo.apps.gitrepositories.data.repository.datasource.GitRepoRemoteDataSource
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams
import br.com.gugo.apps.gitrepositories.domain.repository.GitRepoRepository
import io.reactivex.Observable

class GitRepoDataRepository(
    private val cacheDataSource: GitRepoLocalDataSource,
    private val remoteDataSource: GitRepoRemoteDataSource
) : GitRepoRepository {

    override fun getLiveData(): LiveData<GitRepo.List> {
        return cacheDataSource.gitRepos()
    }

    override fun searchGitRepo(
        params: GitRepoSearchParams?
    ): Observable<GitRepo.List> {
        return remoteDataSource.gitRepos(params).map { it.items }
    }

    override fun cacheData(data: GitRepo.List) {
        cacheDataSource.putRepos(data)
    }

    override fun clearCache() {
        cacheDataSource.clear()
    }
}