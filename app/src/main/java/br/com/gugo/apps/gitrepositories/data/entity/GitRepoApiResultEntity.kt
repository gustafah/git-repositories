package br.com.gugo.apps.gitrepositories.data.entity

import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import com.google.gson.annotations.SerializedName

class GitRepoApiResultEntity(
    @SerializedName("total_count") val totalCount: Int,
    @SerializedName("incomplete_results") val incomplete: Boolean,
    @SerializedName("items") val items: GitRepo.List
)