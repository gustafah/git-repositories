package br.com.gugo.apps.gitrepositories.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GitRepoSearchParams(
    var page: Int = 0,
    var perPage: Int = PER_PAGE,
    var term: String = "",
    var sort: SORTING = SORTING.STARS,
    var order: ORDER = ORDER.DESC,
    var resetCache: Boolean = false
) : Parcelable {
    enum class SORTING(val value: String) {
        STARS("stars"), FORKS("forks")
    }

    enum class ORDER(val value: String) {
        ASC("asc"), DESC("desc")
    }

    companion object {
        const val PER_PAGE: Int = 20
    }
}