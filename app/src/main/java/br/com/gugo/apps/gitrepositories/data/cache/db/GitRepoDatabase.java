package br.com.gugo.apps.gitrepositories.data.cache.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import br.com.gugo.apps.gitrepositories.domain.model.GitRepo;


@Database(entities = {GitRepo.class}, version = 1, exportSchema = false)
public abstract class GitRepoDatabase extends RoomDatabase {

    //SINGLETON
    private static GitRepoDatabase INSTANCE;

    public static GitRepoDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (GitRepoDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            GitRepoDatabase.class, "git_repo_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract GitRepoDao gitRepoDao();
}
