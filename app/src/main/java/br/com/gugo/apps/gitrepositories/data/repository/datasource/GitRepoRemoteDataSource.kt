package br.com.gugo.apps.gitrepositories.data.repository.datasource

import android.annotation.SuppressLint
import br.com.gugo.apps.gitrepositories.data.entity.GitRepoApiResultEntity
import br.com.gugo.apps.gitrepositories.data.network.interfaces.GitRepoApi
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams
import io.reactivex.Observable

class GitRepoRemoteDataSource(private val api: GitRepoApi) {

    @SuppressLint("CheckResult")
    fun gitRepos(params: GitRepoSearchParams?): Observable<GitRepoApiResultEntity> {
        return if (params != null) {
            api.getGitRepos(
                params.term,
                params.page,
                params.perPage,
                params.sort.value,
                params.order.value
            )
        } else {
            api.getGitRepos()
        }
    }
}