package br.com.gugo.apps.gitrepositories.data.cache

import android.arch.lifecycle.LiveData
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo

interface GitRepoCache {

    fun get(): LiveData<GitRepo.List>
    fun clear()
    fun insert(gitRepo: GitRepo)
    fun put(gitRepos: GitRepo.List)

}