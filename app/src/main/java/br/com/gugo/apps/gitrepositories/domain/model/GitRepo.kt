package br.com.gugo.apps.gitrepositories.domain.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = GitRepo.TABLE_NAME)
data class GitRepo(
    @SerializedName("description") var description: String = "",
    @SerializedName("forks_count") var forksCount: Int = 0,
    @PrimaryKey @SerializedName("id") var id: Int = 0,
    @SerializedName("name") var name: String = "",
    @Ignore @SerializedName("owner") var owner: GitRepoOwner,
    @ColumnInfo(name = "ownerName") var ownerName: String = "",
    @ColumnInfo(name = "ownerPicture") var ownerPicture: String = "",
    @SerializedName("stargazers_count") var stargazersCount: Int = 0
) {

    fun starsString() = stargazersCount.toString()
    fun forksString() = forksCount.toString()

    constructor() : this("", 0, 0, "", GitRepoOwner(), "", "", 0)

    class List() : ArrayList<GitRepo>() {
        @Ignore
        constructor(list: kotlin.collections.List<GitRepo>) : this() {
            addAll(list)
        }
    }

    companion object {
        const val TABLE_NAME: String = "git_repo_table"
    }
}