package br.com.gugo.apps.gitrepositories.domain.repository

import android.arch.lifecycle.LiveData
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import br.com.gugo.apps.gitrepositories.domain.model.GitRepoSearchParams
import io.reactivex.Observable

interface GitRepoRepository {
    fun getLiveData(): LiveData<GitRepo.List>
    fun searchGitRepo(params: GitRepoSearchParams?): Observable<GitRepo.List>
    fun cacheData(data: GitRepo.List)
    fun clearCache()
}