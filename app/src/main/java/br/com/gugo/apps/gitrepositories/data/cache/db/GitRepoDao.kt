package br.com.gugo.apps.gitrepositories.data.cache.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import br.com.gugo.apps.gitrepositories.domain.model.GitRepo
import io.reactivex.Flowable

@Dao
interface GitRepoDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGitRepo(gre: GitRepo)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGitRepoList(gre: GitRepo.List)

    @Query("SELECT * FROM ${GitRepo.TABLE_NAME}")
    fun getRepos(): Flowable<List<GitRepo>>

    @Query("DELETE FROM ${GitRepo.TABLE_NAME}")
    fun deleteAll()

}