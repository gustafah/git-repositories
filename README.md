# Git Repositories

Neste projeto, apliquei conceitos que venho estudando há algum tempo.

Alguns deles, confesso, podem ser melhor implementados. Porém acredito ter utilizado as arquiteturas de forma correta.

Neste projeto, me utilizei de
- Kotlin
- Architecture Components (ViewModel + Room + LiveData)
- Clean Architecture structure
- Retrofit + Observable for API
- RxJava2 for Concurrency
- Glide for Image Loading/Caching
- Gson for Parsing API Return

Utilizei técnicas de:
- Infinite Scrolling
- Persistency over Screen Rotation
- Pull to refresh data
- Data Binding for List Adapter

Coisas a melhorar:
- Injection
- UI Tests
- More useful Unit Tests

